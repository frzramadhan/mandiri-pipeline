package ramwibi.mandirisaleskit;

import ramwibi.mandirisaleskit.R;

import ramadhan.utils.Customlog;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

public class MainActivity extends Activity implements OnClickListener {
	/** Called when the activity is first created. */
	protected boolean _active = true;
	protected int _splashTime = 2000; // time to display the splash screen in ms

	LinearLayout a1, a2, a3, a4;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainmenu);

		a1 = (LinearLayout) findViewById(R.id.menuAccount);
		a1.setOnClickListener(this);

		a2 = (LinearLayout) findViewById(R.id.menuList);
		a2.setOnClickListener(this);

		a3 = (LinearLayout) findViewById(R.id.menuNewtask);
		a3.setOnClickListener(this);

		a4 = (LinearLayout) findViewById(R.id.menuHelp);
		a4.setOnClickListener(this);

	}

	public void doMenu(View v) {
		switch (v.getId()) {
		case R.id.menuAccount:
			Customlog.makeText(getApplicationContext(), "Menu Account");
			break;

		case R.id.menuHelp:
			Customlog.makeText(getApplicationContext(), "Menu Help");
			break;

		case R.id.menuList:
			Customlog.makeText(getApplicationContext(), "Menu Task List");
			break;

		case R.id.menuNewtask:
			Customlog.makeText(getApplicationContext(), "Menu Create Task");
			break;

		default:
			Customlog.makeText(getApplicationContext(), "ELSE");
			break;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.menuAccount:
			Customlog.makeText(getApplicationContext(), "Menu Account");
			break;

		case R.id.menuHelp:
			Customlog.makeText(getApplicationContext(), "Menu Help");
			break;

		case R.id.menuList:
			Customlog.makeText(getApplicationContext(), "Menu Task List");
			break;

		case R.id.menuNewtask:
			Customlog.makeText(getApplicationContext(), "Menu Create Task");
			break;

		default:
			Customlog.makeText(getApplicationContext(), "ELSE");
			break;
		}
	}
}
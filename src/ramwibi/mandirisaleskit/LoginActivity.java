package ramwibi.mandirisaleskit;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import ramwibi.mandirisaleskit.R;
import ramwibi.mandirisaleskit.activity.MainActivity;
import ramadhan.utils.Customlog;
import ramadhan.utils.HttpUtils;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity {
	protected TextView txUser;
	protected TextView txPaswd;

	String UNK_MSG = "Unexpected Error";
	int OK = 200;
	int NOK = 0;
	String OTHER = "unks";


	private class Pair {
		public int status;
		public String message;
		public String data;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loginpage);

		txUser = (TextView) findViewById(R.id.editText1);
		txPaswd = (TextView) findViewById(R.id.editText2);

	}

	@SuppressWarnings("unchecked")
	public void doLogin(View view) {

		String usr = txUser.getText().toString().trim();
		String psw = txPaswd.getText().toString().trim();
		if ((usr.length() < 1) || (psw.length() < 1)) {
			// out of range
			Customlog.makeText(this, R.string.popup_user_null,
					Toast.LENGTH_SHORT);
		} else {
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("username", usr);
			params.put("password", psw);

			MyAsyncTask logging = new MyAsyncTask();
			logging.execute(params);
		}

		Customlog.d(Cons.LOGGER, "Username " + usr + " Password " + psw);
	}

	private class MyAsyncTask extends
			AsyncTask<HashMap<String, String>, Void, Pair> {
		//String urlServer = Cons.urlLogin;
		String urlServer = "http://client.farizramadhan.web.id/mandiri/login.php";
		
		public MyAsyncTask(String... urlServer) {
			if (urlServer.length > 0) {
				this.urlServer = urlServer[0];
			}
		}

		private ProgressDialog dialog 	;
		@SuppressWarnings("unused")
		protected Context applicationContext;

		@Override
		protected void onPreExecute() {
			dialog = new ProgressDialog(LoginActivity.this);
			dialog.setCancelable(true);
			dialog.setMessage("Please wait");
			dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			dialog.setProgress(0);
			dialog.show();
		}

		protected Pair doInBackground(HashMap<String, String>... maps) {
			// TODO Auto-generated method stub

			String lineEnd = "\r\n";
			String twoHyphens = "--";
			String boundary = "*****";
			HashMap<String, String> map = maps[0];
			String resultMessage = UNK_MSG;
			int resultStatus = NOK;
			String resultData = OTHER;

			HttpURLConnection connection = null;
			DataOutputStream outputStream = null;

			Pair pair = new Pair();
			pair.message = resultMessage;
			pair.status = resultStatus;
			pair.data = resultData;

			try {
				URL url = new URL(urlServer);
				connection = (HttpURLConnection) url.openConnection();
				// Allow Inputs & Outputs
				connection.setDoInput(true);
				connection.setDoOutput(true);
				connection.setUseCaches(false);

				// Enable POST method
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Connection", "Keep-Alive");
				connection.setRequestProperty("Content-Type",
						"multipart/form-data;boundary=" + boundary);

				outputStream = new DataOutputStream(
						connection.getOutputStream());

				// Send publishId as parameters
				Set<String> keys = map.keySet();
				for (String key : keys) {
					Customlog.d("User SignIn", "POST- " + key);
					outputStream.writeBytes(twoHyphens + boundary + lineEnd);
					outputStream
							.writeBytes("Content-Disposition: form-data; name=\""
									+ key + "\"");
					outputStream.writeBytes(lineEnd);
					outputStream.writeBytes(lineEnd);
					outputStream.writeBytes(map.get(key));
					outputStream.writeBytes(lineEnd);
					outputStream.writeBytes(twoHyphens + boundary + lineEnd);
				}

				outputStream.writeBytes(lineEnd);
				outputStream.writeBytes(twoHyphens + boundary + twoHyphens
						+ lineEnd);

				// Responses from the server (code and message)
				int serverResponseCode = connection.getResponseCode();
				String serverResponseMessage = connection.getResponseMessage();
				Customlog.d(String.valueOf(serverResponseCode),
						serverResponseMessage);
				String responseMessage = HttpUtils
						.getContentFromInputStream(connection.getInputStream());

				Customlog.d("responseMessage", responseMessage);
				JSONObject dataResult;
				try {
					dataResult = new JSONObject(responseMessage);
					resultStatus = dataResult.getInt("status");
					resultMessage = dataResult.getString("message").trim();
					resultData = dataResult.getString("response").trim();

				} catch (JSONException e) {
					// TODO Auto-generated catch block
				} catch (NullPointerException e) {
					// TODO: handle exception
				}

				outputStream.flush();
				outputStream.close();

				pair.message = resultMessage;
				pair.status = resultStatus;
				pair.data = resultData;
				return pair;

			} catch (Exception ex) {
				// Exception handling
				ex.printStackTrace();
				return pair;
			}

		}

		@Override
		protected void onPostExecute(Pair pair) {
			try {
				this.dialog.cancel();
			} catch (Exception e) {
				// TODO: handle exception
			}
			String responseMessage = pair.message.toString();
			int responseStatus = pair.status;
			String responseData = pair.data.toString();

			Customlog.d("onPostExecute", String.valueOf(pair.status));

			responseMessage = responseMessage.trim();
			Customlog.d("MANDIRI", responseMessage);

			if (responseStatus == NOK) {
				popUpOne(2, R.string.toast_error_connection, 0, responseMessage);
			} else if (responseStatus == OK) {
				Customlog.d("MANDIRI", responseData);
				try {
					JSONObject dataProfile = new JSONObject(responseData);
					String userID = dataProfile.getString("uid");
					String userName = dataProfile.getString("nama");
					String userMail = dataProfile.getString("email");
					String userRole = dataProfile.getString("role");
					String userUnit = dataProfile.getString("unit");

					Customlog.d("JSON USER ID", userID);
					Customlog.d("JSON USER NAME", userName);
					Customlog.d("JSON USER NAME", userMail);
					Customlog.d("JSON USER NAME", userRole);
					Customlog.d("JSON USER NAME", userUnit);

					finish();

					Customlog.makeText(LoginActivity.this, "Howdy, Didit!",
							Toast.LENGTH_SHORT);

					Intent i = new Intent(LoginActivity.this,
							MainActivity.class);
					i.putExtra("email", userMail);
					i.putExtra("userid", userID);
					i.putExtra("name", userName);
					// if (userMail.length() < 1 || userMail == null ||
					// userMail.equalsIgnoreCase("null")) {popUpOne(1,
					// R.string.toast_error_connection, 0, "");
					// } else {
					startActivity(i);
					// }

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					Customlog
							.makeText(LoginActivity.this,
									R.string.toast_error_connection,
									Toast.LENGTH_SHORT);
				}

			} else
				Customlog.makeText(LoginActivity.this,
						R.string.toast_error_connection, Toast.LENGTH_SHORT);
		}
	}

	public void popUpOne(int state, int popUpMessage, final int result,
			String alert) {

		final Dialog dialog = new Dialog(LoginActivity.this,
				android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_popup_one);

		TextView MessageAlert = (TextView) dialog
				.findViewById(R.id.messagePopUp);

		LinearLayout no_ = (LinearLayout) dialog.findViewById(R.id.buttonOne);

		if (state == 2) {
			MessageAlert.setText(alert);
		} else {
			MessageAlert.setText(getApplicationContext().getResources()
					.getText(popUpMessage));
		}

		dialog.getWindow().setFlags(LayoutParams.FLAG_NOT_TOUCH_MODAL,
				LayoutParams.FLAG_NOT_TOUCH_MODAL);
		dialog.getWindow().setFlags(LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
				LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

		no_.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();

	}

}

package ramadhan.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.util.Log;

public class HttpUtils {
	static int connectionTimeOut = 10000;
	static int socketTimeOut = 60000;

	public static String webContentGenerator_(String data) {
		String url = "http://";
		url = url + data;
		return url;
	}

	public static String webGet(String url, String context,
			WebResultListener listener) {
		BufferedReader in = null;
		// Log.d("WebGetURL", url);
		try {

			HttpGet request = new HttpGet();

			// request.addHeader("Accept-Encoding", "gzip");

			HttpParams httpParameters = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					connectionTimeOut);
			HttpConnectionParams.setSoTimeout(httpParameters, socketTimeOut);
			HttpClient client = new DefaultHttpClient(httpParameters);
			request.setURI(new URI(url));
			HttpResponse response = client.execute(request);

			InputStream instream = response.getEntity().getContent();
			// if (response.getFirstHeader("Content-Encoding") != null
			// && response.getFirstHeader("Content-Encoding").getValue()
			// .equalsIgnoreCase("gzip")) {
			// instream = new GZIPInputStream(instream);
			// }

			// in = new BufferedReader(new
			// InputStreamReader(response.getEntity()
			// .getContent()), 8096);
			in = new BufferedReader(new InputStreamReader(instream), 8096);

			StringBuffer sb = new StringBuffer("");
			String line = "";
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			in.close();
			String page = sb.toString();
			if (listener != null) {
				listener.onResult(page, context, null);
			}
			return page;
		} catch (Exception e) {
			// System.out.println("exception di httputils:" + e.getMessage() +
			// "("
			// + e.getClass() + ")");
			e.printStackTrace();
			if (listener != null) {
				listener.onResult(new String(""), context, null);
			}
			return null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String webPost(String url, String context,
			WebResultListener listener, Map<String, String> postData)
			throws Exception {
		BufferedReader in = null;
		try {
			HttpParams httpParameters = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					connectionTimeOut);
			HttpConnectionParams.setSoTimeout(httpParameters, socketTimeOut);
			HttpClient client = new DefaultHttpClient(httpParameters);

			HttpPost request = new HttpPost();
			request.setURI(new URI(url));
			List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			for (String key : postData.keySet()) {
				postParameters.add(new BasicNameValuePair(key, postData
						.get(key)));
			}
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(
					postParameters);
			request.setEntity(formEntity);

			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			in.close();
			String page = sb.toString();
			if (listener != null) {
				listener.onResult(page, context, null);
			}
			return page;
		} catch (Exception e) {
			System.out.println("exception di httputils:" + e.getMessage() + "("
					+ e.getClass() + ")");
			return null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static String getWebContent(String url) {
		BufferedReader in = null;
		String result = "";
		try {
			HttpGet request = new HttpGet();
			HttpParams httpParameters = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					connectionTimeOut);
			HttpConnectionParams.setSoTimeout(httpParameters, socketTimeOut);
			HttpClient client = new DefaultHttpClient(httpParameters);
			request.setURI(new URI(url));
			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()), 8096);
			StringBuffer sb = new StringBuffer("");
			String line = "";
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			in.close();
			result = sb.toString();

		} catch (Exception e) {
			Customlog.d("httputils:getWebContent", e.getMessage());
			System.out.println("exception di httputils:" + e.getMessage() + "("
					+ e.getClass() + ")");
			return result;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	public static String webPost(String url, String context,
			Map<String, String> postData) throws Exception {
		BufferedReader in = null;
		int connectionTimeOut = 10000;
		int socketTimeOut = 60000;
		try {
			HttpParams httpParameters = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters,
					connectionTimeOut);
			HttpConnectionParams.setSoTimeout(httpParameters, socketTimeOut);
			HttpClient client = new DefaultHttpClient(httpParameters);

			HttpPost request = new HttpPost();
			request.setURI(new URI(url));
			List<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			for (String key : postData.keySet()) {
				postParameters.add(new BasicNameValuePair(key, postData
						.get(key)));
			}
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(
					postParameters);
			request.setEntity(formEntity);

			HttpResponse response = client.execute(request);
			in = new BufferedReader(new InputStreamReader(response.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			while ((line = in.readLine()) != null) {
				sb.append(line);
			}
			in.close();
			String page = sb.toString();
			return page;
		} catch (Exception e) {
			System.out.println("exception di httputils:" + e.getMessage() + "("
					+ e.getClass() + ")");
			return null;
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static InputStream OpenHttpConnection(String strURL)
			throws IOException {
		InputStream inputStream = null;
		URL url = new URL(strURL);
		URLConnection conn = url.openConnection();

		try {
			HttpURLConnection httpConn = (HttpURLConnection) conn;
			httpConn.setRequestMethod("GET");
			httpConn.connect();

			if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				inputStream = httpConn.getInputStream();
			}
		} catch (Exception ex) {
		}
		return inputStream;
	}

	public static String getContentFromInputStream(InputStream stream)
			throws IOException {
		BufferedReader rd = null;
		StringBuilder sb = null;
		String line = null;
		// read the result from the server
		rd = new BufferedReader(new InputStreamReader(stream));
		sb = new StringBuilder();

		while ((line = rd.readLine()) != null) {
			sb.append(line + '\n');
		}
		return sb.toString();
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	public static String resolveRedirect(String url)
			throws ClientProtocolException, IOException {
		HttpParams httpParameters = new BasicHttpParams();
		HttpClientParams.setRedirecting(httpParameters, false);

		HttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpGet httpget = new HttpGet(url);
		HttpContext context = new BasicHttpContext();

		HttpResponse response = httpClient.execute(httpget, context);

		// If we didn't get a '302 Found' we aren't being redirected.
		if (response.getStatusLine().getStatusCode() != HttpStatus.SC_MOVED_TEMPORARILY)
			throw new IOException(response.getStatusLine().toString());

		Header loc[] = response.getHeaders("Location");
		return loc.length > 0 ? loc[0].getValue() : null;
	}

}

package ramadhan.utils;

import ramwibi.mandirisaleskit.R;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class Customlog {
	
	static boolean LOG_MODE = true;

	public static void d(String logTag, String logMessage) {
		if (LOG_MODE) {
			android.util.Log.d(logTag, logMessage);
		}
	}

	public static void w(String logTag, Exception exceptionMessage) {
		if (LOG_MODE) {
			android.util.Log.w(logTag,
					android.util.Log.getStackTraceString(exceptionMessage));
		}
	}

	public static void w(String logTag, String errorMessage) {
		if (LOG_MODE) {
			android.util.Log.w(logTag, errorMessage);
		}
	}

	public static void i(String logTag, String infoMessage) {
		if (LOG_MODE) {
			android.util.Log.i(logTag, infoMessage);
		}
	}

	public static void i(String logTag, Exception exceptionMessage) {
		if (LOG_MODE) {
			android.util.Log.i(logTag,
					android.util.Log.getStackTraceString(exceptionMessage));
		}
	}

	public static void makeText(Context context, String textToast, int duration) {
		// TODO Auto-generated method stub
		try {
			LayoutInflater inflater = LayoutInflater.from(context
					.getApplicationContext());

			View layout = inflater.inflate(R.layout.toast_custom,
					(ViewGroup) ((Activity) context)
							.findViewById(R.id.toast_layout));

			TextView text = (TextView) layout.findViewById(R.id.toast_text_1);
			text.setText(textToast);

			Toast toast = new Toast(context);
			// toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.setDuration(duration);
			toast.setView(layout);
			toast.show();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void makeText(Context context, int textToast, int duration) {
		// TODO Auto-generated method stub

		try {
			LayoutInflater inflater = LayoutInflater.from(context
					.getApplicationContext());

			View layout = inflater.inflate(R.layout.toast_custom,
					(ViewGroup) ((Activity) context)
							.findViewById(R.id.toast_layout));
			((TextView) layout.findViewById(R.id.toast_text_1)).setText(context
					.getResources().getString(textToast));
			Toast toast = new Toast(context);
			// toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.setDuration(duration);
			toast.setView(layout);
			toast.show();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static void makeText(Context context, String message) {
		try {
			LayoutInflater inflater = LayoutInflater.from(context
					.getApplicationContext());

			View layout = inflater.inflate(R.layout.toast_custom,
					(ViewGroup) ((Activity) context)
							.findViewById(R.id.toast_layout));

			TextView text = (TextView) layout.findViewById(R.id.toast_text_1);
			text.setText(message);

			Toast toast = new Toast(context);
			// toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			toast.setDuration(Toast.LENGTH_SHORT);
			toast.setView(layout);
			toast.show();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}

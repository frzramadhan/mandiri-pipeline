package ramadhan.utils;

public interface WebResultListener {
	void onResult(String data, String context, Object addData);
}
